//
//  Cours.h
//  iitapp
//
//  Created by IITASR on 07/11/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cours : NSObject


@property (nonatomic,strong) NSString* name;
@property int duration;

@end
