//
//  main.m
//  iitapp
//
//  Created by IITASR on 24/10/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
