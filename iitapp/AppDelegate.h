//
//  AppDelegate.h
//  iitapp
//
//  Created by IITASR on 24/10/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

