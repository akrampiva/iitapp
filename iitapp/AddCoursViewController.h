//
//  AddCoursViewController.h
//  iitapp
//
//  Created by IITASR on 07/11/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListcourViewController.h"

@interface AddCoursViewController : UIViewController
{
    
    IBOutlet UITextField* cours_name;
    
}

@property (nonatomic,strong) ListcourViewController* previousListCoursViewController;
@end
