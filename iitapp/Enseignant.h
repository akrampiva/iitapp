//
//  Enseignant.h
//  iitapp
//
//  Created by IITASR on 07/11/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Enseignant : NSObject


@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* familyname;
@property (nonatomic,strong) NSDate* birthday;


@end
