//
//  ListcourViewController.h
//  iitapp
//
//  Created by IITASR on 24/10/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListcourViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    
    IBOutlet UITableView* tableView;
}
@property (nonatomic, strong) NSMutableArray* listCours;
@end
