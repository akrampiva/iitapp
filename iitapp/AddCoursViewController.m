//
//  AddCoursViewController.m
//  iitapp
//
//  Created by IITASR on 07/11/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import "AddCoursViewController.h"
#import "Cours.h"

@interface AddCoursViewController ()

@end

@implementation AddCoursViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addCours:(id)sender{
    
    Cours* c1 = [[Cours alloc] init];
    [c1 setName:cours_name.text];
    
    [_previousListCoursViewController.listCours addObject:c1];
    
    [self.navigationController popViewControllerAnimated:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
