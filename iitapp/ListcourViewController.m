//
//  ListcourViewController.m
//  iitapp
//
//  Created by IITASR on 24/10/2015.
//  Copyright © 2015 IITASR. All rights reserved.
//

#import "ListcourViewController.h"
#import "Cours.h"
#import "AddCoursViewController.h"

@interface ListcourViewController ()

@end

@implementation ListcourViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"home";
    
    Cours* c1 = [[Cours alloc] init];
    //c1.name = @"SRI";
    [c1 setName:@"SRI"];
    
    Cours* c2 = [[Cours alloc] init];
    [c2 setName:@"GI"];
    
    
    _listCours = [[NSMutableArray alloc] init];
    [_listCours addObject:c1];
    [_listCours addObject:c2];
}


-(void)viewWillAppear:(BOOL)animated{
    [tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listCours count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [[_listCours objectAtIndex:indexPath.row] name];
    return cell;
}


-(IBAction)goToAddCours:(id)sender{
    
    AddCoursViewController* addCoursView = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCoursViewController"];
    addCoursView.previousListCoursViewController = self;
    [self.navigationController showViewController:addCoursView sender:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
